app.controller('AppCtrl', function($scope) {
	
})

.controller('WelcomeCtrl', function($scope, $state) {
  $scope.user= {};
  $scope.onSubmit = function(user) {
    $state.go('verify');
  }
})


.controller('VerifyCtrl', function($scope, $state) {
  
  $scope.user = {};

  $scope.onSubmit = function(user) {
    $state.go('app.latest_news');
  }
})

.controller('NewsListCtrl', function($scope, Fetch, $ionicLoading, $state, $ionicHistory) {

	$ionicHistory.clearHistory()

	var nlc =this;
	nlc.data = [];

	nlc.doRefresh = function() {
		fetchLatestNews();	
	}

	$ionicLoading.show().then(function(){
    	fetchLatestNews();	
    })
	
	function fetchLatestNews(){
		Fetch.top20news().success(function(data) {
			nlc.data = [];
			nlc.data = data.story;
			$ionicLoading.hide();
			$scope.$broadcast('scroll.refreshComplete');
		}).error(function(error) {
			$ionicLoading.hide();
			$scope.$broadcast('scroll.refreshComplete');
		})
	}

	nlc.viewDetail = function(data) {
		$state.go("news_detail", {details: JSON.stringify(data)});
	}
})

.controller('CategoriesCtrl', function($scope, Fetch, $ionicLoading, $state) {
	var cctrl = this;

	$ionicLoading.show().then(function(){
		fetchCategories();
	})

	function fetchCategories() {
		Fetch.getCategoriesList().success(function(data) {
			cctrl.items = data.category;
			$ionicLoading.hide();
		}).error(function(error) {
			$ionicLoading.hide();
			console.log(JSON.stringify(error))
		})
	}

	cctrl.goToSubcategories = function(data){
		console.log(data.name)
		$state.go("subcategories", {id: data.id, name: data.name})
	}

})

.controller('NewsDetailCtrl', function($scope, $state, $stateParams,$ionicHistory, $ionicPopup) { 
	$scope.goBack = function() {
      $ionicHistory.goBack();
    };

   	$scope.detail = JSON.parse($stateParams.details);

   	$scope.ratingCallback = function(rating, index) {
	        console.log('Selected rating is : ', rating, ' and the index is : ', index);
	      };
   	
   	$scope.showRatingPopUp = function() {
   		$scope.ratingsObject =  {
    	    iconOn: 'ion-ios-star',    //Optional
    	    iconOff: 'ion-ios-star-outline',   //Optional
    	    iconOnColor: 'rgb(200, 200, 100)',  //Optional
    	    iconOffColor:  'rgb(200, 100, 100)',    //Optional
    	    rating:  1, //Optional
    	    minRating:1,    //Optional
    	    readOnly: false, //Optional
    	    callback: function(rating) {    //Mandatory
    	      $scope.ratingsCallback(rating);
    	    }
          };
   			
   		$scope.ratingCallback = function(rating, index) {
	        console.log('this Selected rating is : ', rating, ' and the index is : ');
	      };

   		var RatePopup = $ionicPopup.show({
   			template : '<ionic-ratings style="font-size: 50px;" ratingsobj="ratingsObject"></ionic-ratings>',
   			title: 'Enter your Rating',
   			scope: $scope,
   			buttons: [
		      { text: 'Cancel' },
		      {
		        text: '<b>Submit</b>',
		        type: 'button-balanced',
		        onTap: function(rating, index) {
		            $scope.ratingCallback(rating, index);
		        }
		      }
    		]
   		});	
   	}

	
})

.controller('SubcategoriesCtrl', function($scope, $stateParams, $ionicHistory, Fetch, $ionicLoading, $state) {
	$scope.title = $stateParams.name;

	$ionicLoading.show().then(function() {
		getSubcategories($stateParams.id)	
	});
	
	function getSubcategories(id) {
		Fetch.getSubCategories(id).success(function(data) {
			console.log(data.category)
			$scope.items = data.category;
			$ionicLoading.hide();
		}).error(function(error) {
			alert(error);
			$ionicLoading.hide();
		})
	}

	$scope.goBack = function() {
		  $ionicHistory.goBack();
		};

	$scope.showStories =function(catid, subid, name) {
		$state.go("subcategories_stories", {catid : catid, subid: subid,name : name});
	}
	
})

.controller('StoriesCtrl', function($scope, $stateParams, $ionicHistory, $ionicLoading, Fetch) {
	$scope.title = $stateParams.name;

	$scope.goBack = function() {
	      $ionicHistory.goBack();
	   };

	$ionicLoading.show().then(function() {
		getStories($stateParams.catid, $stateParams.subid);
	})

	$scope.doRefresh = function(){
		getStories($stateParams.catid, $stateParams.subid);
	}

	function getStories(catid, subid) {
		Fetch.getStoriesOfSubcategory($stateParams.catid, $stateParams.subid)
			.success(function(data) {
				console.log(data);
				$scope.news = data.story;
				$scope.$broadcast('scroll.refreshComplete');
				$ionicLoading.hide();
			}).error(function(data) {
				$ionicLoading.hide();
				$scope.$broadcast('scroll.refreshComplete');
			})
	}
})