app.factory('Fetch', function($http, Config) {

	var Fetch = {};

	Fetch.top20news = function() {
		return $http.get(Config.url + '/topurl');
	};

	Fetch.getCategoriesList = function() {
		return $http.get(Config.url + '/categoryList')
	}

	Fetch.getSubCategories = function(catid) {
		return $http.get(Config.url + '/subcategory/'+catid)
	}

	Fetch.getStoriesOfSubcategory = function(catid, subid) {
		return $http.get(Config.url + '/storyCategory/'+catid+"/"+subid)
	}

	return Fetch;

 })